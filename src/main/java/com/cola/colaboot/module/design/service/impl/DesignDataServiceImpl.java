package com.cola.colaboot.module.design.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cola.colaboot.module.design.mapper.DesignDataMapper;
import com.cola.colaboot.module.design.pojo.DesignData;
import com.cola.colaboot.module.design.service.DesignDataService;
import org.springframework.stereotype.Service;

@Service
public class DesignDataServiceImpl extends ServiceImpl<DesignDataMapper, DesignData> implements DesignDataService {
    @Override
    public IPage<DesignData> pageList(DesignData designData, Integer pageNo, Integer pageSize) {
        LambdaQueryWrapper<DesignData> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DesignData::getState,1);
        queryWrapper.eq(StringUtils.isNotBlank(designData.getTitle()),DesignData::getTitle,designData.getTitle());
        queryWrapper.orderByDesc(DesignData::getCreateTime);
        Page<DesignData> page = new Page<>(pageNo, pageSize);
        return page(page, queryWrapper);
    }
}
