package com.cola.colaboot.module.design.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SqlExecuteMapper {

    @Select("${sqlStr}")
    List<Map<String,Object>> executeList(@Param(value = "sqlStr") String sqlStr);

    @Select("${sqlStr}")
    Map<String,Object> executeObject(@Param(value = "sqlStr") String sqlStr);
}
