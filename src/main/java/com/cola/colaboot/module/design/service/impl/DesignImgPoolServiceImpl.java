package com.cola.colaboot.module.design.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cola.colaboot.module.design.mapper.DesignImgPoolMapper;
import com.cola.colaboot.module.design.pojo.DesignImgPool;
import com.cola.colaboot.module.design.service.DesignImgPoolService;
import org.springframework.stereotype.Service;

@Service
public class DesignImgPoolServiceImpl extends ServiceImpl<DesignImgPoolMapper, DesignImgPool> implements DesignImgPoolService {
    @Override
    public IPage<DesignImgPool> pageList(DesignImgPool pool, Integer pageNo, Integer pageSize) {
        Page<DesignImgPool> page = new Page<>(pageNo, pageSize);
        return getBaseMapper().pageList(page, pool);
    }
}
