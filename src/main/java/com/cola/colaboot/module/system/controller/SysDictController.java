package com.cola.colaboot.module.system.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.module.system.service.SysDictService;
import com.cola.colaboot.module.system.pojo.SysDict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysDict")
public class SysDictController {

    @Autowired
    private SysDictService sysDictService;

    @GetMapping("/pageList")
    public Res<?> pageList(SysDict sysDict,
           @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
           @RequestParam(name="pageSize", defaultValue="10") Integer pageSize){
        return Res.ok(sysDictService.pageList(sysDict,pageNo,pageSize));
    }

    @PostMapping("/saveOrUpdate")
    public Res<?> saveOrUpdate(SysDict sysDict){
        sysDictService.saveOrUpdate(sysDict);
        return Res.ok("success");
    }
}
